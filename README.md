# tof

Monolithic firmware and software repo for the GAPS time of flight system. Contains code for DRS4 readout board and TOF physics trigger.